import json
import os
from datetime import datetime

import dill
import pandas as pd


path = os.environ.get('PROJECT_PATH', '.')
model_path = f'{path}/data/models/cars_pipe_202209290219.pkl'
json_path = f'{path}/data/test/'
pred_df = pd.DataFrame()
prediction_list = list()


def predict():

    # Загрузка обученной модели
    with open(model_path, 'rb') as file:
        model = dill.load(file)
    # Предсказание для всех объектов в папке data/test
    for json_files in os.listdir(json_path):
        with open(json_path + json_files, 'r') as json_file:
            json_data = json.load(json_file)
            df = pd.DataFrame(json_data, index=[0])
            df_id = df['id'][0]
            model_prediction = model.predict(df)[0]
            pred_list = list([df_id, model_prediction])
            prediction_list.append(pred_list)

    pred_df = pd.DataFrame(prediction_list, columns=['car_id', 'pred'])
    pred_df.to_csv(f'{path}/data/predictions/preds_{datetime.now().strftime("%Y%m%d%H%M")}.csv', index=False)


if __name__ == '__main__':
    predict()
